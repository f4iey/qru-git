# QRU package manager

The **Quiet Repo Utility** (QRU) is a simple package manager for F6KGL OS written in shell.

In fact, this shell script is more like a `pacman` helper to make f6kgl cutsom repo easier to use.

## Installation

A PKGBUILD is available for this operation. Simply run:
```sh
mkdir qru-build
cd qru-build
wget https://gitlab.com/f4iey/f6kgl-pkgbuilds/-/raw/main/qru-git/PKGBUILD
makepkg -si
```
### Removal

If you want to remove QRU for any reason, simply do:
```sh
pacman -R qru-git
```
## Usage

To use the package manager, enabling root is needed to install, remove and update packages from the f6kgl repo, e.g:
```sh
sudo qru update
sudo qru install cqrlog-bin
sudo qru remove cqrlog-bin
```
